import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import Siswa from 'App/Models/Siswa'


export default class SiswasController {
  public async index({view}: HttpContextContract) {
      const siswa = await Siswa.all();

      return view.render('siswa',  {
        data: siswa,
        active: 'active'
      });
  }

  public async create({view}: HttpContextContract) {
      return view.render('add')
  }

  public async store({response, request}: HttpContextContract) {
      const roles = schema.create({
        nama: schema.string({}, [
          rules.required(),
          rules.minLength(5),
          rules.maxLength(255)
        ]),
        nis: schema.number([
          rules.unique({table: 'siswas', column: 'nis'})
        ]),
        email: schema.string({}, [
          rules.email()
        ]),
        visimisi: schema.string({}, [
          rules.required()
        ])
      })
      
      const validData = await request.validate({ schema: roles })

      const siswa = await Siswa.create(validData)
      await siswa.save()

      console.log(siswa.$isPersisted) // true

      response.redirect('/siswa')
  } 

  public async show({params,view}: HttpContextContract) {
      const id = params.id
      const details = await Siswa.findBy('id', id)

      return view.render('detail', {
        data: details,
        img: "../../public/profile.png"
      })
  }

  public async edit({params, view}: HttpContextContract) {
      const id = params.id
      const data = await Siswa.findBy('id', id)

      return view.render('edit', {
        data: data
      })
  }

  public async update({params, request,response}: HttpContextContract) {
      const id = params.id;
      const siswa = await Siswa.findBy('id', id);

      const roles = schema.create({
        nama: schema.string({}, [
          rules.required(),
          rules.minLength(5),
          rules.maxLength(255)
        ]),
        nis: schema.number([
          rules.unique({table: 'siswas', column: 'nis'}),
          rules.maxLength(9)
        ]),
        email: schema.string({}, [
          rules.email()
        ]),
        visimisi: schema.string({}, [
          rules.required()
        ])
      })
      
      const validData = await request.validate({ schema: roles })

      siswa?.merge(validData);
      await siswa?.save();

      response.redirect('/siswa');

  }

  public async destroy({params, response}: HttpContextContract) {
      const id = params.id
      const siswa = await Siswa.findOrFail(id)

      await siswa.delete()

      response.redirect('/siswa')
  }
}
