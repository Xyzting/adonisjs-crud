import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Siswa from 'App/Models/Siswa'


export default class extends BaseSeeder {
  public async run () {
    await Siswa.createMany([
      {
        nama: 'Reyhan Fathir',
        nis: 121456789,
        email: 'reyhan123@gnail.com',
        visimisi: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque dignissimos at incidunt ab ipsa reprehenderit non aliquam, dolores quis autem sequi porro laborum ducimus quam, voluptatum corrupti ratione eos fuga.'
      },
      {
        nama: 'Umar Syaifudin',
        nis: 121246812,
        email: 'umarsyaf@gnail.com',
        visimisi: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque dignissimos at incidunt ab ipsa reprehenderit non aliquam, dolores quis autem sequi porro laborum ducimus quam, voluptatum corrupti ratione eos fuga.'
      },
      {
        nama: 'Joko Sundoyo',
        nis: 121541238,
        email: 'jokoslibaw@gnail.com',
        visimisi: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloremque dignissimos at incidunt ab ipsa reprehenderit non aliquam, dolores quis autem sequi porro laborum ducimus quam, voluptatum corrupti ratione eos fuga.'
      },
    ])
  }
}
